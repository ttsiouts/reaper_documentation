# Preemptible Servers Prototype

## Purpose

The purpose of this, is to introduce the prototype Reaper service responsible
for preemptying instances, as decided by the Nova Team PTG [1].

The implementation of this prototype service will help to identify the needed
changes in Nova to optimize its performance.

The service's functionality is based on the most recent spec [2].

## Prerequisites

### Tagging and Quota

Users should be able to choose if a server is preemptible at creation time. A
limitation of this prototype is that mutable tagging will be used where as the
preemptible property should never change after the creation of the instance.

There are a few aspects for enforcing quota:

* Users who have filled their regular quota want to use preemtable for
  additional resources
* There is the need to limit which projects get to use preemtible servers. The
  ideal for this would be a way to limit how many preemptible resources could
  be used on top of the existing quota.

Currently, in Nova, there is no link in between the preemptible tag and the
quota.

#### Initial work

As a starting point and in order to focus on the reaper's functionality, we
will use a project with unlimited quota for resources and control only the
number of preemptible instances that can be spawned by using the existing nova
mechanism (instances quota). This project will be allocated only with flavors
bearing the preemptible property.

This way:
    - the servers that belong to this project, will be tagged as preemptibles
      since they were spawned using a preemptible flavor.
    - We can limit the usage of preemptible resources for a project. Without
      having maximum set exact limits for each resource class, we can
      calculate the maximum quota for each project with the formula
      max_value * instances, where:

        - max_value: the maximum value for this resource class in the
                     allocated flavors
        - instances: the number of the allowed instances for this project


Another benefit from this is that there there is no need to hack the Nova API
to avoid quota checks just for preemptible VMs.

#### Follow Up work

After the successful introduction of the new service, the follow up work will
be focused on identifying the ways we can calculate the preemptible quota on
top of the existing ones.

Thoughts:
    - Expose a custom resource class using a nested resource provider for each
      compute node. Then add the resource in the preemptible flavors and use 
      the new granular resource requests from placement. Enforce quota only
      for the custom resource class.
    - Can we use traits? If we could use traits to tag a subset of resource in
      each resource provider.
    - Again ending up on the same problem..


### Alternatives

As an alternative for tagging, we could tag the whole project where the
servers belong to. This way we would be able to distinguish preemptibles from
non-preemptibles base on the project they are originated. Downside of this, we
need info from keystone.

Also scheduler hints could be used to tag VMs as preemptibles.

## Reaper Service

Under the scope of the Preemptible Servers feature, a Reaper service has to be
implemented. This component needs to be external to Nova.

Reaper's main duty is to orchestrate Preemptible Servers. Specifically, it's
the reaper's responsibility to select the preemptible servers that have to be
"culled", in order to free up the requested resources for the creation of
non-preemptible instances.

The service's proposed data flow consists of the following:

- Triggered by Nova at NoValidHost events

  When a boot request for a non-preemptible instance fails due to the fact
  that no valid hosts were returned from Placement API, Nova, will place a
  request for cleaning up resources to the reaper. The request contains
  the same resource specifications as the boot request that failed in the
  first place.

- The Reaper service queries Preemptible VMs' usages

  At this point the service has to find out where the requested resources can
  be freed up. The service will gather information on the allocated resources
  to already running preemptible servers. For this prototype, the selection of
  the servers will be done greedily based on a configuration option specifying
  the number of alternative slots for each requested instance. Effectively
  the service will try to free up:
        - at maximum: requested slots * alternative slots (per instance)
        - at least: requested slots

- Strategies for selecting preemptibles

  Currently we have two strategies (drivers) for selecting instances:

    * chance driver
        This driver maps servers to hosts. A valid host is selected
        randomly and in a number of preconfigured retries, it tries to find
        the instances that have to be culled in order to have the requested
        resources available.

    * strict driver
        This driver maps servers to flavors and hosts and tries to find the
        best matching existing combination of flavors. The purpose of the
        preemptibles existence is to eliminate the idling resources, so the
        best matching combination is the one that leaves as less as possible
        resources unused. Once the flavor combination is found, servers that
        match to this combination are selected for culling.

- "Culling" Preemptible Instances

  The reaper service places delete server requests to the Nova API, in order
  to delete the (preemptible) servers, selected for "culling".

- Response to Scheduler

  The reaper's response will be used directly from the scheduler in order to
  claim the freed up resources. In other words, the service will respond in
  the same way as the Placement's response, when asked for allocation
  candidates (allocations, provider summaries). This will save time since an
  extra call to the Placement for this reason is not needed.

### Scenarios

#### Booting a Preemptible Server

As mentioned, the preemptible project will have unlimited quota for the
resource classes and the user has limited number of instances to spawn.

##### Resources available

(Trivial) Since there are available resources, booting a server is expected to
succeed.

##### No resources available

(Trivial) While booting a preemptible server, if the Placement API returns no
valid candidate host, then the procedure fails. No request is placed to the
reaper in this case.

#### Booting a Non-Preemptible Server

Quota accounting is applied for the non-preemptible servers, so a project's
quota has to not be exceeded in order to have the ability to boot a server
successfully.

##### Resources available

(Trivial) Since there are available resources, booting a server is expected to
succeed.

##### No resources available

- Since there are no available resources, the Placement API returns no valid
  candidates for hosting the new Server.
- At this time, Nova places a request to the Reaper, in order to free up the
  resources needed for the non-preemptible server.
- The reaper service selects the preemptible servers using one of the
  strategies above (configuration option) and proceeds with their deletion.
- The service reports back to the scheduler with a response the form of
  allocation candidates.

## References

[1] http://lists.openstack.org/pipermail/openstack-dev/2017-September/122258.html
[2] https://review.openstack.org/#/c/438640/
